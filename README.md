# Software

## Kubernetes-in-Docker

KIND is used to simulate a local multi-node Kubernetes cluster to spin things up in.

> Reference URL: https://kind.sigs.k8s.io/
