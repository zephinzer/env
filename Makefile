NAME=zenv
KIND_VERSION=0.8.1

docker_start:
	@docker-compose version
	-@docker swarm init
	docker-compose up -d
docker_stop:
	@docker-compose version
	docker-compose down
k8s_start:
	@kind version
	kind create cluster \
		--config ./kubernetes/kind/config.yaml \
		--name $(NAME)
k8s_stop:
	@kind version
	kind delete cluster \
		--name $(NAME)

install_kind_linux:
	mkdir -p ./.downloads
	curl -Lo ./.downloads/kind-v$(KIND_VERSION) 'https://github.com/kubernetes-sigs/kind/releases/download/v$(KIND_VERSION)/kind-linux-amd64'
	chmod +x ./.downloads/kind-v$(KIND_VERSION)
	mkdir -p /opt/kind/v$(KIND_VERSION)
	mv ./.downloads/kind-v$(KIND_VERSION) /opt/kind/v$(KIND_VERSION)/kind
	-rm /usr/local/bin/kind
	ln -s /opt/kind/v$(KIND_VERSION)/kind /usr/local/bin/kind
install_kind_macos:
	brew install kind
install_kind_windows:
	choco install kind
